#!/usr/bin/python

#################################################################
#	Bibliotecas y configuracion de ambiente
#################################################################
import glob, os
from pyparsing import *
import urllib
import numpy as np
import pickle
import sys  
import Stemmer 
from file2plaintext import *

reload(sys)  
sys.setdefaultencoding('utf8')

############################################################################
#	Limpieza de archivo(eliminacion de tags, caracteres no utilizados
#############################################################################
def limpiarArchivoHtml(file):
     
    targetPage = open(file,'r')
    targetHTML = targetPage.read()
    targetPage.close()

    # first pass, strip out tags and translate entities
    firstPass = (htmlComment | scriptBody | commonHTMLEntity | 
				         anyTag | anyClose ).transformString(targetHTML)
    # first pass leaves many blank lines, collapse these down
    repeatedNewlines = LineEnd() + OneOrMore(LineEnd())
    repeatedNewlines.setParseAction(replaceWith("\n\n"))
    secondPass = repeatedNewlines.transformString(firstPass)

    parsedText=open(file+'.txt','w')
    parsedText.write(secondPass)
    parsedText.close()

def limpiarArchivoTexto(file):
    ruta = "./"+file
    textoLimpio = document_to_text(file,ruta)
    parsedText=open(file+'.txt','w')
    parsedText.write(textoLimpio)
    parsedText.close()

#################################################################
#		tokenizacion
#################################################################
def tokenizar(file):
    archivo=open(file+'.txt','r')
    texto = archivo.read()
    stemmer = Stemmer.Stemmer('spanish')
    texto = texto.decode('utf-8')
    tokensNoNormalizados = texto.split()
    tokens = stemmer.stemWords(tokensNoNormalizados)
    archivo.close()
    
    lista = []

    # Conteo de terminos
    conteoPalabra = {}
    for token in tokens:
        if token not in conteoPalabra:
            conteoPalabra[token] = 1
        else:
            conteoPalabra[token] += 1
    
    for token in tokens:
        if not token in palReconocidas:
            palReconocidas.append(token)
            lista.append(token)
            lista.append([file,conteoPalabra[token]])
            diccionario.append(lista)
            lista = []
        else:
            for termino in diccionario:
                if(termino[0] == token):
                    archivoEnTermino = False
                    for archivo in termino[1:]:
                        if archivo[0] == file:
                            archivoEnTermino = True
                            break
                    if not archivoEnTermino:
                        termino.append([file,conteoPalabra[token]])
                    break
            


################################################################
#		Supresion de tags
# Hecho con base en htmlStripper.py 
# Sample code for stripping HTML markup tags and scripts from 
#  HTML source files.
# Copyright (c) 2006, Paul McGuire
################################################################

################################################################
removeText = replaceWith(" ")
scriptOpen,scriptClose = makeHTMLTags("script")
scriptBody = scriptOpen + SkipTo(scriptClose) + scriptClose
scriptBody.setParseAction(removeText)

anyTag,anyClose = makeHTMLTags(Word(alphas,alphanums+":_"))
anyTag.setParseAction(removeText)
anyClose.setParseAction(removeText)
htmlComment.setParseAction(removeText)

commonHTMLEntity.setParseAction(replaceHTMLEntity)


#################################################################
#  Variables globales
#################################################################
os.chdir(".")
diccionario = []
palReconocidas = []
N = 0

for file in glob.glob("*.html"):
    print('\n\n\n'+file)
    limpiarArchivoHtml(file)
    tokenizar(file) 
    N = N+1

for file in glob.glob("*.pdf"):
    print('\n\n\n'+file)
    limpiarArchivoTexto(file)
    tokenizar(file) 
    N = N+1

for file in glob.glob("*.doc"):
    print('\n\n\n'+file)
    limpiarArchivoTexto(file)
    tokenizar(file) 
    N = N+1

for file in glob.glob("*.docx"):
    print('\n\n\n'+file)
    limpiarArchivoTexto(file)
    tokenizar(file) 
    N = N+1

for file in glob.glob("*.odt"):
    print('\n\n\n'+file)
    limpiarArchivoTexto(file)
    tokenizar(file) 
    N = N+1

#################################################################
#  Guardado de estructura diccionario
#################################################################
indice = open('indice.txt', 'w')
diccionario.append(N)
pickle.dump(diccionario, indice)
indice.close()
