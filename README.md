# Preambulo

Debido a la gran cantidad de informacion que existe en la web, mucha informacion de gran valor producida en las universidades de nuestro pais puede ser valorada con poca relevancia por los motores de busqueda tradicionales como Google o Bing. De ahi surge la importancia del motor de busqueda CRU (Costa Rican Universities) el cual unicamente se enfoca en el material producido en universidades de Costa Rica

# Teoria

Filminas del curso Recuperacion de Informacion
Profesor: Diego Villalba
Escuela de Ciencias de la Computacion e Informatica
Universidad de Costa Rica

# Funcionamiento del sistema de generacion del indice


 +-----------+   +----------------+   +-----------+
 |           |   |                |   |           |
 |  Crawler  +--->Proprocesamiento+---> Indizado  |
 |           |   |  de archivos   |   |           |
 +-----------+   |                |   +-----------+
                 +----------------+

### Crawler: 
Almacena archivos html, pdf, doc, docx y odt que se encuentran en la web basado en las semillas dadas.

### Preprosesamiento de archivos:
Elimina etiquetas y demas simbolos propios del formato del archivo dejando solo texto relevante.

### Indizado
Crea las 'listas de postings'.

# Instalacion

sudo apt-get install easy-install python-pip libxml2 libxslt libxml2-dev libxslt1-dev python-dev

sudo pip install scrapy

sudo easy-install docx pdfminer
