import scrapy

from cru.items import PageItem

class CRUSpider(scrapy.Spider):
     
     # Nombre del crawler
     # Este nombre es el que se utiliza para ejecutar el crawler 'scrapy crawl cru_sp'
     name = "cru_sp"
     
     # Lista de urls semilla
     start_urls = [ 
         "http://www.orh.ucr.ac.cr/documentos/manuales",
         "http://www.kerwa.ucr.ac.cr/",
         "http://www.ucr.ac.cr/",
         "http://www.tec.ac.cr/",
         "http://www.una.ac.cr/",
         "http://www.uned.ac.cr/",
         "http://www.utn.ac.cr/",
         "http://www.usanmarcos.ac.cr/",
         "http://ufidelitas.ac.cr/",
         "http://www.invenio.org/",
         "http://www.ulatina.ac.cr/",
         "http://www.uam.ac.cr/",
         "http://www.catie.ac.cr/es/productos-y-servicios",
         "http://ufidelitas.ac.cr/bachilleratos-y-licenciaturas/",
         "http://ufidelitas.ac.cr/",
         "http://www.upi.ac.cr/",
         "http://www.uca.ac.cr/",
         "http://archivomusical.ucr.ac.cr/"
     ]   
     
     # Set vacio para almacenar los urls visitados
     urls_visitados = set()
              

     # Funcion principal del crawler
     def parse(self, response):
         
         for href in response.xpath("//li//@href").extract():
             # Verifica si el archivo no es html
             if '.pdf' in href or '.doc' in href or '.docx' in href or '.ppt' in href or '.pptx' in href or '.odp' in href or '.xls' in href or '.xlsx' in href or '.zip' in href or '.rar' in href:
                 yield scrapy.Request(href, callback=self.guardar_archivo)
             # Verifica si el archivo es html
             elif len(href) > 0 and href not in self.urls_visitados:
                 # Verfica si el href tiene palabras o urls no deseados
                 if href[0] != '#' and href[0] != '/' and not 'www.campus.una.ac.cr.html' in href and not 'osg.ucr.ac.cr' in href and not 'catalogo' in href and not 'empleo' in href and not 'aspx' in href and not 'javascript' in href and not 'tel' in href and not 'inicio' in href and 'php' not in href:
                     yield scrapy.Request(href, callback=self.parsear_pagina)
 
         for href in response.xpath("//a/@href").extract() :
             # Verifica si el archivo no es html
             if '.pdf' in href or '.doc' in href or '.docx' in href or '.ppt' in href or '.pptx' in href or '.odp' in href or '.xls' in href or '.xlsx' in href or '.zip' in href or '.rar' in href:
                 yield scrapy.Request(href, callback=self.guardar_archivo)
             # Verifica si el archivo es html
             elif len(href) > 0 and href not in self.urls_visitados:
                 # Verfica si el href tiene palabras o urls no deseados
                 if href[0] != '#' and href[0] != '/' and not 'www.campus.una.ac.cr.html' in href and not 'osg.ucr.ac.cr' in href and not 'catalogo' in href and not 'empleo' in href and not 'aspx' in href and not 'javascript' in href and not 'tel' in href and not 'inicio' in href and 'php' not in href:
                     yield scrapy.Request(href, callback=self.parsear_pagina)

     def parsear_pagina(self, response):
         self.urls_visitados.add(response.url)
         if hasattr(response, 'xpath'):
             title = response.xpath('a/text()').extract()
             text = response.xpath("//text()").extract()
             item = PageItem()
             item['title'] = title
             item['url'] = response.url
             item['text'] = text
 
             archivo = response.url.split("/")[-2] + '.html'
             with open(archivo, 'wb') as f:
                 f.write(response.body)
             yield item

     def guardar_archivo(self, response):
         archivo = response.url.split("/")[-1]
         with open(archivo, "wb") as f:
             f.write(response.body)	
     
     def save_doc(self, response):
         archivo = response.url.split("/")[-1]
         with open(archivo, "wb") as f:
             f.write(response.body)	
