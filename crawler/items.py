import scrapy

class PageItem(scrapy.Item):
   title = scrapy.Field()
   url = scrapy.Field()
   text = scrapy.Field()
